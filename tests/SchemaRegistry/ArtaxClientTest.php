<?php

/**
 * Copyright 2019 Jaumo GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace AvroTest\SchemaRegistry;

use Amp\Artax\Client;
use Amp\Artax\Request;
use Amp\Artax\Response;
use Amp\ByteStream\InputStream;
use Amp\ByteStream\Message;
use Amp\Loop;
use Amp\Success;
use Avro\SchemaRegistry\ArtaxClient;
use Avro\SchemaRegistry\ClientError;
use Avro\SchemaRegistry\Model\Error;
use Avro\Serialization\Schema\Denormalizer;
use Avro\Serialization\Schema\Serializer;
use PHPStan\Testing\TestCase;
use PHPUnit\Framework\MockObject\MockObject;

class ArtaxClientTest extends TestCase
{
    private const BASE_URL = 'http://example.org';

    /**
     * @var ArtaxClient
     */
    private $client;

    /**
     * @var Client|MockObject
     */
    private $httpClientMock;

    /**
     * @var Denormalizer|MockObject
     */
    private $serializerMock;

    protected function setUp(): void
    {
        $this->httpClientMock = $this->createMock(Client::class);
        $this->serializerMock = $this->createMock(Serializer::class);

        $this->client = new ArtaxClient(self::BASE_URL, $this->httpClientMock);
    }

    public function testGetRegisteredSchemaIdWithKnownSchema(): void
    {
        $request = (new Request(self::BASE_URL . '/subjects/foo', 'POST'))
            ->withAddedHeader('Accept', 'application/vnd.schemaregistry.v1+json')
            ->withBody('{"schema":"\"serialized_schema\""}');

        $this->mockResponse($request, '{"id": 42}');

        Loop::run(function () {
            $id = yield $this->client->getRegisteredSchemaId('foo', '"serialized_schema"');
            $this->assertSame(42, $id);
        });
    }

    public function testGetRegisteredSchemaIdWithUnknownSchema(): void
    {
        $request = (new Request(self::BASE_URL . '/subjects/foo', 'POST'))
            ->withAddedHeader('Accept', 'application/vnd.schemaregistry.v1+json')
            ->withBody('{"schema":"\"serialized_schema\""}');

        $this->mockResponse($request, '{"error_code": 40403}');

        Loop::run(function () {
            $id = yield $this->client->getRegisteredSchemaId('foo', '"serialized_schema"');
            $this->assertSame(null, $id);
        });
    }

    public function testRegisterSchema()
    {
        $request = (new Request(self::BASE_URL . '/subjects/foo/versions', 'POST'))
            ->withAddedHeader('Accept', 'application/vnd.schemaregistry.v1+json')
            ->withBody('{"schema":"\"serialized_schema\""}');

        $this->mockResponse($request, '{"id": 42}');

        Loop::run(function () {
            $id = yield $this->client->registerSchema('foo', '"serialized_schema"');
            $this->assertSame(42, $id);
        });
    }

    /**
     * @dataProvider exampleErrors
     *
     * @param int $code
     * @param string $reason
     * @param string $exceptionMessage
     */
    public function testApiErrors(int $code, string $reason, string $exceptionMessage): void
    {
        $request = (new Request(self::BASE_URL . '/subjects/foo/versions', 'POST'))
            ->withAddedHeader('Accept', 'application/vnd.schemaregistry.v1+json')
            ->withBody('{"schema":"\"serialized_schema\""}');

        $this->mockResponse($request, \sprintf('{"error_code": %d, "message": "%s"}', $code, $reason));

        $this->expectException(Error::class);
        $this->expectExceptionCode($code);
        $this->expectExceptionMessage($exceptionMessage);

        Loop::run(function () {
            yield $this->client->registerSchema('foo', '"serialized_schema"');
        });
    }

    public function testInvalidJsonError(): void
    {
        $request = (new Request(self::BASE_URL . '/subjects/foo/versions', 'POST'))
            ->withAddedHeader('Accept', 'application/vnd.schemaregistry.v1+json')
            ->withBody('{"schema":"\"serialized_schema\""}');

        $this->mockResponse($request, '{[');

        $this->expectException(ClientError::class);
        $this->expectExceptionMessage('Failed to parse JSON');

        Loop::run(function () {
            yield $this->client->registerSchema('foo', '"serialized_schema"');
        });
    }

    /**
     * @return array
     */
    public function exampleErrors(): array
    {
        return [
            [50001, 'foobar', 'Error in the backend datastore: foobar'],
            [50001, '', 'Error in the backend datastore'],
            [12345, '', 'unknown error'],
            [40403, 'test', 'Schema not found: test'],
        ];
    }

    private function mockResponse(Request $request, string $responseBodyStr): void
    {
        $this
            ->httpClientMock
            ->method('request')
            ->with($this->equalTo($request))
            ->willReturnCallback(function (Request $request) use ($responseBodyStr) {
                $responseBody = $this->createMock(InputStream::class);
                $responseBody->method('read')->willReturn(new Success($responseBodyStr), new Success(null));

                $responseMock = $this->createMock(Response::class);
                $responseMock->method('getBody')->willReturn(new Message($responseBody));

                return new Success($responseMock);
            });
    }
}
