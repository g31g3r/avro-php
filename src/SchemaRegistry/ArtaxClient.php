<?php

/**
 * Copyright 2019 Jaumo GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Avro\SchemaRegistry;

use Amp\Artax\Client;
use Amp\Artax\DefaultClient;
use Amp\Artax\Request;
use Amp\Artax\Response;
use function Amp\call;
use Amp\Promise;
use Avro\SchemaRegistry\Model\Error;
use Safe\Exceptions\JsonException;
use Safe\Exceptions\StringsException;

class ArtaxClient implements AsyncClient
{
    private const ACCEPT_MIME_TYPE = 'application/vnd.schemaregistry.v1+json';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $baseUri;

    public function __construct(
        string $baseUri,
        ?Client $client = null
    ) {
        $this->baseUri = \rtrim($baseUri, '/');
        $this->client = $client ?? new DefaultClient();
    }

    public function getRegisteredSchemaId(string $subject, string $schema): Promise
    {
        return call(function () use ($subject, $schema) {
            try {
                $json = yield $this->jsonRequest(
                    self::PATH_POST_SCHEMA_REGISTERED,
                    [$subject],
                    'POST',
                    ['schema' => $schema]
                );

                return $json['id'];
            } catch (ClientError $e) {
                if (\in_array($e->getCode(), [Error::SUBJECT_NOT_FOUND, Error::SCHEMA_NOT_FOUND], true)) {
                    return null;
                }

                throw $e;
            }
        });
    }

    public function registerSchema(string $subject, string $schema): Promise
    {
        return call(function () use ($subject, $schema) {
            $json = yield $this->jsonRequest(
                self::PATH_POST_REGISTER_SCHEMA,
                [$subject],
                'POST',
                ['schema' => $schema]
            );

            return $json['id'];
        });
    }

    public function getSchema(int $id): Promise
    {
        return call(function () use ($id) {
            $json = yield $this->jsonRequest(
                self::PATH_GET_SCHEMA,
                [$id]
            );

            return $json['schema'];
        });
    }

    /**
     * @param string $path
     * @param mixed ...$params
     * @return Promise
     */
    private function jsonRequest(
        string $path,
        array $params,
        string $method = 'GET',
        array $body = []
    ): Promise {
        return call(function () use ($path, $params, $method, $body) {
            /** @var Response $response */
            $response = yield $this->client->request(
                $this->buildRequest($path, $params, $method, \Safe\json_encode($body))
            );

            $raw = yield $response->getBody()->read();
            try {
                $json = \Safe\json_decode($raw, true);
                if (Error::isError($json)) {
                    throw Error::fromResponse($json);
                }

                return $json;
            } catch (JsonException $e) {
                throw ClientError::jsonParseFailed($raw, $e);
            }
        });
    }

    /**
     * @param string $path
     * @param mixed ...$params
     * @return Request
     * @throws ClientError
     */
    private function buildRequest(string $path, array $params, string $method, string $body): Request
    {
        try {
            $uri = $this->baseUri . \Safe\sprintf($path, ...$params);

            $request = (new Request($uri, $method))
                ->withAddedHeader('Accept', self::ACCEPT_MIME_TYPE)
                ->withBody($body);

            return $request;
        } catch (StringsException $e) {
            throw ClientError::buildRequestFailed($e);
        }
    }
}
