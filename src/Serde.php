<?php

/**
 * Copyright 2019 Jaumo GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Avro;

use Amp\Promise;
use Avro\Model\Protocol\Protocol;
use Avro\Model\Schema\Schema;
use Avro\SchemaRegistry\Serializer as SchemaRegistrySerializerInterface;
use Avro\Serialization\Message\DefaultSerializer as MessageSerializer;
use Avro\Serialization\Message\Serializer as MessageSerializerInterface;
use Avro\Serialization\Protocol\DefaultSerializer as ProtocolSerializer;
use Avro\Serialization\Protocol\MessageDenormalizer;
use Avro\Serialization\Protocol\MessageNormalizer;
use Avro\Serialization\Protocol\ProtocolDenormalizer;
use Avro\Serialization\Protocol\ProtocolNormalizer;
use Avro\Serialization\Protocol\Serializer as ProtocolSerializerInterface;
use Avro\Serialization\Schema\ArrayDenormalizer;
use Avro\Serialization\Schema\ArrayNormalizer;
use Avro\Serialization\Schema\ChainDenormalizer;
use Avro\Serialization\Schema\ChainNormalizer;
use Avro\Serialization\Schema\DefaultSerializer as SchemaSerializer;
use Avro\Serialization\Schema\EnumDenormalizer;
use Avro\Serialization\Schema\EnumNormalizer;
use Avro\Serialization\Schema\FixedDenormalizer;
use Avro\Serialization\Schema\FixedNormalizer;
use Avro\Serialization\Schema\LogicalTypeNormalizer;
use Avro\Serialization\Schema\MapDenormalizer;
use Avro\Serialization\Schema\MapNormalizer;
use Avro\Serialization\Schema\PrimitiveDenormalizer;
use Avro\Serialization\Schema\PrimitiveNormalizer;
use Avro\Serialization\Schema\RecordDenormalizer;
use Avro\Serialization\Schema\RecordFieldDenormalizer;
use Avro\Serialization\Schema\RecordFieldNormalizer;
use Avro\Serialization\Schema\RecordNormalizer;
use Avro\Serialization\Schema\ReferenceDenormalizer;
use Avro\Serialization\Schema\ReferenceNormalizer;
use Avro\Serialization\Schema\Serializer as SchemaSerializerInterface;
use Avro\Serialization\Schema\UnionDenormalizer;
use Avro\Serialization\Schema\UnionNormalizer;

final class Serde
{
    /**
     * @var SchemaSerializerInterface
     */
    private static $schemaSerializer;

    /**
     * @var ProtocolSerializerInterface
     */
    private static $protocolSerializer;

    /**
     * @var MessageSerializerInterface
     */
    private static $messageSerializer;

    private function __construct()
    {
    }

    public static function encodeMessage(Schema $schema, $message): string
    {
        self::init();

        return self::$messageSerializer->serialize($schema, $message);
    }

    public static function encodeMessageWithSchemaRegistry(
        Schema $schema,
        $message,
        string $subject,
        SchemaRegistrySerializerInterface $serializer
    ): Promise {
        self::init();

        return $serializer->serialize(
            $subject,
            $schema,
            $message,
            self::$messageSerializer,
            self::$schemaSerializer
        );
    }

    public static function decodeMessageWithSchemaRegistry(
        string $message,
        SchemaRegistrySerializerInterface $serializer,
        ?Schema $schema = null
    ): Promise {
        self::init();

        return $serializer->deserialize(
            $message,
            $schema,
            self::$messageSerializer,
            self::$schemaSerializer
        );
    }

    public static function decodeMessage(Schema $schema, string $message)
    {
        self::init();

        return self::$messageSerializer->deserialize($message, $schema);
    }

    public static function dumpSchema(Schema $schema): string
    {
        self::init();

        return self::$schemaSerializer->serialize($schema);
    }

    public static function dumpCanonicalSchema(Schema $schema): string
    {
        self::init();

        return self::$schemaSerializer->serialize($schema, true);
    }

    public static function parseSchema(string $json): Schema
    {
        self::init();

        return self::$schemaSerializer->deserialize($json);
    }

    public static function dumpProtocol(Protocol $protocol): string
    {
        self::init();

        return self::$protocolSerializer->serialize($protocol);
    }

    public static function parseProtocol(string $json): Protocol
    {
        self::init();

        return self::$protocolSerializer->deserialize($json);
    }

    private static function init(): void
    {
        if (null === self::$schemaSerializer) {
            self::$schemaSerializer = new SchemaSerializer(
                new ChainNormalizer([
                    new ArrayNormalizer(),
                    new EnumNormalizer(),
                    new FixedNormalizer(),
                    new LogicalTypeNormalizer(),
                    new MapNormalizer(),
                    new PrimitiveNormalizer(),
                    new RecordFieldNormalizer(),
                    new RecordNormalizer(),
                    new UnionNormalizer(),
                    new ReferenceNormalizer(),
                ]),
                new ChainDenormalizer([
                    new ArrayDenormalizer(),
                    new EnumDenormalizer(),
                    new FixedDenormalizer(),
                    new MapDenormalizer(),
                    new PrimitiveDenormalizer(),
                    new RecordDenormalizer(),
                    new RecordFieldDenormalizer(),
                    new UnionDenormalizer(),
                    new ReferenceDenormalizer(),
                ])
            );
        }

        if (null === self::$protocolSerializer) {
            self::$protocolSerializer = new ProtocolSerializer(
                new ProtocolNormalizer(
                    self::$schemaSerializer,
                    new MessageNormalizer(self::$schemaSerializer)
                ),
                new ProtocolDenormalizer(
                    self::$schemaSerializer,
                    new MessageDenormalizer(self::$schemaSerializer)
                )
            );
        }

        if (null === self::$messageSerializer) {
            self::$messageSerializer = new MessageSerializer();
        }
    }
}
